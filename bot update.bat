@echo off
title Update TheKianBot
echo Are You Sure. (Press Control+C To Cancel)
pause
echo Start Delete Current Bot...
del "Bot Working Directory\*.jar"
echo Done Delete Current Bot! 
echo Start Update from Code...
call gradlew build
echo Done Update From Code!
echo Start Copying Files from Build to Working Enviorment...
copy "build\libs\*.*" "Bot Working Directory"
echo Done Copying Files from Build to Working Enviorment!
echo Bot Ready To Launch!
pause