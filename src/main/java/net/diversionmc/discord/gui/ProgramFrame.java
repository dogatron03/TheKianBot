package net.diversionmc.discord.gui;

import net.diversionmc.discord.TheKianBot;
import net.diversionmc.discord.dogaapi.LogFile;

import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ProgramFrame extends Frame implements WindowListener {

    private static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(2);
    public static TextArea console;
    public static TextField input;
    public static Button exit;
    public static List<String> consoleLines;
    public static ProgramFrame instance;
    private static int exited;

    public ProgramFrame() {
        super("TheKianBot");
        instance = this;
        LogFile.setup();
        setSize(600, 300);
        setBackground(SystemColor.control);
        setLocationRelativeTo(null);
        setLayout(new FlowLayout());
        addWindowListener(this);
        console = new TextArea("");
        consoleLines = new ArrayList<>();
        consoleLines.add("Welcome To TheKianBot!");
        input = new TextField(">> ");
        add(console);
        input.setColumns(60);
        input.addActionListener(new GUIListener());
        add(input);
        exit = new Button("Close");
        exit.addActionListener(new GUIListener());
        exit.setVisible(false);
        add(exit);
        setVisible(true);
        exited = 0;
        final Runnable writeToConsole = new Runnable() {
            @Override
            public void run() {
                String s = "";
                for (String consoleLine : consoleLines) {
                    s = s + consoleLine + "\n";
                }
                if(!(console.getText() + s).equals(console.getText())) {
                    console.setText(s);
                }
            }
        };
        scheduler.scheduleAtFixedRate(writeToConsole, 20, 20, TimeUnit.MILLISECONDS);
    }

    public static void addConsoleLines(String line) {
        consoleLines.add(line);
    }

    public static void exit() {
        exited = 1;
        TheKianBot.getInstance().getTextChannels().get(0).sendMessage("Bot Shutting Down!").complete();
        new LogFile(LogFile.getLogItems());
        ProgramFrame.addConsoleLines("Bot Shutting Down!");
        ProgramFrame.input.setEditable(false);
        exit.setVisible(true);
        instance.setVisible(false);
        instance.setVisible(true);
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
        if (exited == 0) exit();
        else {
            dispose();
            System.exit(0);
        }
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
