package net.diversionmc.discord.gui;

import net.diversionmc.discord.TheKianBot;
import net.diversionmc.discord.dogaapi.LogFile;
import net.diversionmc.discord.utils.Youtube;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUIListener implements ActionListener {

    public static void onCommand(String s) {
        String[] f = s.split(" ", 2);
        String s1 = f[0];
        String s2 = "";
        if (f.length != 1) {
            s2 = f[1];
        }
        if (s1.equalsIgnoreCase("exit")) {
            ProgramFrame.exit();
        } else if (s1.equalsIgnoreCase("log")) {
            new LogFile(LogFile.getLogItems());
        } else if (s1.equalsIgnoreCase("say")) {
            String[] s3 = s2.split(" ");
            String s5 = "";
            for (String s4 : s3) {
                s5 = s5 + s4 + " ";
            }
            TheKianBot.getInstance().getTextChannels().get(0).sendMessage(s5).complete();
        } else if (s1.equalsIgnoreCase("upload")){
            String[] s3 = s2.split(" ");
            String s5 = s3[0];
            TheKianBot.getInstance().getTextChannels().get(0).sendMessage(Youtube.getLatestVideo(s5)).complete();

        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == ProgramFrame.input) {
            onCommand(e.getActionCommand().substring(3));
            ProgramFrame.input.setText(">> ");
            ProgramFrame.input.setCaretPosition(3);
        } else if(e.getSource() == ProgramFrame.exit){
            System.exit(0);
        }
    }
}

