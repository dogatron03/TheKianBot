package net.diversionmc.discord;

import net.diversionmc.discord.dogaapi.CommandCalledEvent;
import net.diversionmc.discord.dogaapi.LogFile;
import net.diversionmc.discord.gui.ProgramFrame;
import net.diversionmc.discord.managment.CommandManagment;
import net.diversionmc.discord.managment.music.PlayerControl;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.exceptions.RateLimitedException;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import javax.security.auth.login.LoginException;
import java.sql.Timestamp;

public class TheKianBot extends ListenerAdapter {

    private static JDA instance;


    public static void main(String[] args) {
        try {
            new ProgramFrame();
            instance = new JDABuilder(AccountType.BOT).setToken("MzM5MDAyMzM5NzczOTA2OTQ0.DFdoDA.TDqHqLhnEK_0gsnoY5Q_txywlVw").buildAsync();
            instance.addEventListener(new TheKianBot());
        } catch (LoginException e) {
            ProgramFrame.addConsoleLines("An error occurred whilst connecting to the discord servers!");
            ProgramFrame.exit();
        } catch (RateLimitedException e) {
            ProgramFrame.addConsoleLines("An error occurred whilst connecting to the discord servers!");
            ProgramFrame.exit();
        }
        ProgramFrame.addConsoleLines("Connected to the discord servers!");
        PlayerControl.setupPlayerControl();
        CommandManagment.createCommands();
        LogFile.setup();
    }

    public static JDA getInstance() {
        return instance;
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent e) {
        if (e.isFromType(ChannelType.TEXT)) {
            LogFile.logItem("[" + new Timestamp(System.currentTimeMillis()).toString() + "][" + e.getGuild().getName() + "] [" + e.getChannel().getName() + "] [" + e.getAuthor().getName() + "] " + e.getMessage().getContent());
            ProgramFrame.addConsoleLines("[" + new Timestamp(System.currentTimeMillis()).toString() + "][" + e.getGuild().getName() + "] [" + e.getChannel().getName() + "] [" + e.getAuthor().getName() + "] " + e.getMessage().getContent());
            CommandCalledEvent.onMessageRecived(e.getJDA(), e.getResponseNumber(), e.getMessage());
            PlayerControl.onCommand(e);
        }
    }

}
