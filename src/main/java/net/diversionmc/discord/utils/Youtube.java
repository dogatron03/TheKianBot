package net.diversionmc.discord.utils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.diversionmc.discord.gui.ProgramFrame;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class Youtube {

    public static String searchYoutube(String q) {
        String x = q.replaceAll(" ", "+");
        try {
            String s = "https://www.googleapis.com/youtube/v3/search?part=snippet&q=" + x + "&type=video&key=" + getDevKeyFromFile();
            return "youtube.com/watch?v=" + getVideoFromSearch(s);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getDevKeyFromFile() {
        File settings = null;
        try {
            settings = new File("settings\\uploads.txt");
            if(!settings.exists()) {
                settings.createNewFile();
            }
        } catch (IOException e) {
            System.out.println("1");
            e.printStackTrace();
        }
        Scanner s;
        try {
            s = new Scanner(settings);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("2");
            return null;
        }
        String st = s.nextLine();
        if (st.startsWith("devKey:")) {
            return st.substring(7);
        } else {
            return null;
        }
    }

    public static String getLatestVideo(String username) {
        String out = "";
        try {
            String myChannelName = username;
            String myDevKey = getDevKeyFromFile();
            if (myDevKey == null) {
                System.out.println("Invalid Dev Key. Fix in settings\\uploads.txt!");
                return "";
            }

            String xURL = "https://www.googleapis.com/youtube/v3/channels?part=contentDetails&forUsername=" + myChannelName + "&key=" + myDevKey;
            String sURL = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=" + getChannelUploadsID(xURL) + "&key=" + myDevKey;
            out = "http://youtube.com/watch?v=" + getVideoFromID(sURL);
        } catch (IOException e) {
            ProgramFrame.addConsoleLines("An error occurred when requesting youtube video!");
        }
        return out;
    }

    public static String getLatestVideoID(String id) {
        String out = "";
        try {
            String myDevKey = getDevKeyFromFile();
            if (myDevKey == null) {
                System.out.println("Invalid Dev Key. Fix in settings\\uploads.txt!");
                return "";
            }
            String xURL = "https://www.googleapis.com/youtube/v3/channels?part=contentDetails&id=" + id + "&key=" + myDevKey;
            String sURL = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=" + getChannelUploadsID(xURL) + "&key=" + myDevKey;
            out = "http://youtube.com/watch?v=" + getVideoFromID(sURL);
            System.out.println(out);
        } catch (IOException e) {
            ProgramFrame.addConsoleLines("An error occurred when requesting youtube video!");
        }
        return out;
    }

    private static String getVideoFromID(String sURL) throws IOException {
        URL url = new URL(sURL);
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.connect();
        JsonParser jp = new JsonParser();
        JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
        JsonObject rootobj = root.getAsJsonObject();
        JsonObject x = ((JsonObject) rootobj.getAsJsonArray("items").get(0));
        JsonObject s = x.getAsJsonObject("snippet");
        JsonObject y = s.getAsJsonObject("resourceId");
        JsonElement z = y.get("videoId");
        return z.getAsString();
    }

    private static String getVideoFromSearch(String sURL) throws IOException {
        URL url = new URL(sURL);
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.connect();
        JsonParser jp = new JsonParser();
        JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
        JsonObject rootobj = root.getAsJsonObject();
        JsonObject x = ((JsonObject) rootobj.getAsJsonArray("items").get(0));
        JsonObject y = x.getAsJsonObject("id");
        JsonElement z = y.get("videoId");
        return z.getAsString();
    }

    public static String getKiansLatestVideo() {
        String out = "";
        try {
            String myChannelName = "TheKian2134";
            String myDevKey = getDevKeyFromFile();
            if (myDevKey == null) {
                System.out.println("Invalid Dev Key. Fix in settings\\uploads.txt!");
                return "";
            }

            String xURL = "https://www.googleapis.com/youtube/v3/channels?part=contentDetails&forUsername=" + myChannelName + "&key=" + myDevKey;
            String sURL = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=" + getChannelUploadsID(xURL) + "&key=" + myDevKey;
            URL url = new URL(sURL);
            HttpURLConnection request = (HttpURLConnection) url.openConnection();
            request.connect();
            JsonParser jp = new JsonParser();
            JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
            JsonObject rootobj = root.getAsJsonObject();
            JsonObject x = ((JsonObject) rootobj.getAsJsonArray("items").get(0));
            JsonObject s = x.getAsJsonObject("snippet");
            JsonObject y = s.getAsJsonObject("resourceId");
            JsonElement z = y.get("videoId");
            out = "http://youtube.com/watch?v=" + z.getAsString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return out;
    }


    public static String getChannelUploadsID(String xURL) throws IOException {
        URL url = new URL(xURL);
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.connect();
        JsonParser jp = new JsonParser();
        JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
        JsonObject rootobj = root.getAsJsonObject();
        JsonObject x = ((JsonObject) rootobj.getAsJsonArray("items").get(0));
        JsonObject s = x.getAsJsonObject("contentDetails");
        JsonObject y = s.getAsJsonObject("relatedPlaylists");
        String z = y.get("uploads").getAsString();
        return z;
    }


}
