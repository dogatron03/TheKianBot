package net.diversionmc.discord.dogaapi;

import net.diversionmc.discord.gui.ProgramFrame;

import java.util.LinkedList;
import java.util.List;

public class DiscordCommand implements CommandInterface {

    private static List<DiscordCommand> commands;
    private String name;

    public DiscordCommand(String name) {
        commands.add(this);
        this.name = name;
    }

    public DiscordCommand(String name, String... aliases) {
        commands.add(this);
        for (String s : aliases) {
            new DiscordCommand(s);
        }
        this.name = name;
    }


    public static void setupCommands() {
        commands = new LinkedList<>();
    }

    public static void onMessageRecived(CommandCalledEvent e) {
        commands.stream().filter(f -> f.name.equalsIgnoreCase(e.getCommandName())).forEach(abc -> {
            abc.onCommandCalled(e);
            ProgramFrame.addConsoleLines(abc.name);
        });
    }


    public void onCommandCalled(CommandCalledEvent e) {
        e.getChannel().sendMessage("Placeholder Command. If this message shows, the bot is in development or something has gone wrong.").complete();
    }
}
