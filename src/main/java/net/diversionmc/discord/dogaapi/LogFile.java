package net.diversionmc.discord.dogaapi;


import net.diversionmc.discord.gui.ProgramFrame;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class LogFile {

    private static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private static List<String> logItems;

    @SuppressWarnings("depreciation")
    public LogFile(List<String> logItems) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        File file = new File("logs\\" + timestamp.getYear() + "-" + timestamp.getMonth() + "-" + timestamp.getDate() + "-" + timestamp.getHours() + "-" + timestamp.getMinutes() + ".log");
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter p;
        try {
            p = new PrintWriter(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }
        logItems.forEach(x -> p.println(x));
        p.close();
        logItems.clear();
        ProgramFrame.addConsoleLines("Made new LogFile!");
    }

    public static void logItem(String toLog) {
        logItems.add(toLog);
    }

    public static void setup() {
        logItems = new ArrayList<>();
        logItems.clear();
        final Runnable writerToLog = new Runnable() {
            @Override
            public void run() {
                if (!logItems.isEmpty()) {
                    new LogFile(logItems);
                    System.out.println("Made Log File @ " + new Timestamp(System.currentTimeMillis()));
                }
                System.out.println("Nothing to write to log, not making log file!");
            }
        };
        scheduler.scheduleAtFixedRate(writerToLog, 15, 15, TimeUnit.MINUTES);
        new File("logs\\").mkdir();
    }

    public static List<String> getLogItems() {
        return logItems;
    }
}
