package net.diversionmc.discord.dogaapi;

import net.dv8tion.jda.client.entities.Group;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.message.GenericMessageEvent;

public class CommandCalledEvent extends GenericMessageEvent {
    private final Message message;
    private final String[] messageRaw;
    private final String commandName;
    private final String args;


    private CommandCalledEvent(JDA api, long responseNumber, Message message) {
        super(api, responseNumber, message.getIdLong(), message.getChannel());
        this.message = message;
        messageRaw = message.getContent().split(" ", 2);
        commandName = messageRaw[0].substring(1);
        if(messageRaw.length ==2) {
            args = messageRaw[1];
        } else args = null;
    }

    public static void onMessageRecived(JDA api, long responseNumber, Message message) {
        if (message.getContent().substring(0, 1).equalsIgnoreCase("!")) {
            CommandCalledEvent e = new CommandCalledEvent(api, responseNumber, message);
            DiscordCommand.onMessageRecived(e);
        }
    }

    public Message getMessage() {
        return message;
    }

    public String getCommandName() {
        return commandName;
    }

    public String getCommandArgs(){
        return args;
    }

    public User getAuthor() {
        return message.getAuthor();
    }

    public Member getMember() {
        return isFromType(ChannelType.TEXT) ? getGuild().getMember(getAuthor()) : null;
    }

    public PrivateChannel getPrivateChannel() {
        return message.getPrivateChannel();
    }

    public Group getGroup() {
        return message.getGroup();
    }

    public TextChannel getTextChannel() {
        return message.getTextChannel();
    }

    public Guild getGuild() {
        return message.getGuild();
    }


}
