package net.diversionmc.discord.dogaapi;

public class MessageCommand extends DiscordCommand {


    private String message;


    public MessageCommand(String name, String message) {
        super(name);
        this.message = message;
    }

    public MessageCommand(String name, String message, String... aliases) {
        super(name);
        for(String s : aliases){
            new MessageCommand(s, message);
        }
        this.message = message;
    }

    @Override
    public void onCommandCalled(CommandCalledEvent e) {
        if (!e.getAuthor().isBot()) {
            e.getChannel().sendMessage(message).complete();
            e.getMessage().delete();
        }
    }
}
