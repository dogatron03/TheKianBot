package net.diversionmc.discord.managment;

import net.diversionmc.discord.dogaapi.DiscordCommand;
import net.diversionmc.discord.dogaapi.MessageCommand;
import net.diversionmc.discord.managment.commands.GetUploads;
import net.diversionmc.discord.managment.commands.GetUploadsID;
import net.diversionmc.discord.managment.commands.Live;
import net.diversionmc.discord.utils.Youtube;

public class CommandManagment {


    public static void createCommands() {
        //Kian, to add a command you have to make a new MessageCommand(); The first argument is the primary name, the second is the text for it to return. The third and onwards are aliases. These are optional and you can add as many (or little) as you like. :-)
        DiscordCommand.setupCommands();
        new GetUploads("uploads");
        new GetUploadsID("uploadsid");
        new Live("live", "livestream");
        new MessageCommand("ping", "Pong!");
        new MessageCommand("youtube", "http://youtube.com/TheKian2134 is my YouTube channel!", "yt", "videos");
        new MessageCommand("instagram", "https://www.instagram.com/thekian2134/ is my Instagram!", "insta", "ig");
        new MessageCommand("twitter", "https://twitter.com/RealTheKian2134 is my Twitter!", "tweet");
        new MessageCommand("facebook", "https://www.facebook.com/TheKian2134/ is my Facebook!", "fb");
        new MessageCommand("bot", "Bot coded by Dogatron03! \n API by https://github.com/DV8FromTheWorld/ \n Music Bot by https://github.com/DV8FromTheWorld/ with tweaks from Dogatron03", "botinfo", "info", "thekianbot");
        new MessageCommand("latestvideo", "My latest and greatest video is: " + Youtube.getKiansLatestVideo(), "video", "latest");
        new MessageCommand("ultimategamers", "test");

    }

}
