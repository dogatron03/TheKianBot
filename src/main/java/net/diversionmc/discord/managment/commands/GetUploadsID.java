package net.diversionmc.discord.managment.commands;

import net.diversionmc.discord.dogaapi.CommandCalledEvent;
import net.diversionmc.discord.dogaapi.DiscordCommand;
import net.diversionmc.discord.utils.Youtube;

public class GetUploadsID extends DiscordCommand {


    public GetUploadsID(String name) {
        super(name);
    }


    @Override
    public void onCommandCalled(CommandCalledEvent e) {
        e.getChannel().sendMessage("Channel ID " + e.getCommandArgs() + "'s latest upload on youtube is: " + Youtube.getLatestVideoID(e.getCommandArgs())).complete();
    }
}
