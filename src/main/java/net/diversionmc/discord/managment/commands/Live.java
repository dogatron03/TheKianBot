package net.diversionmc.discord.managment.commands;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import net.diversionmc.discord.TheKianBot;
import net.diversionmc.discord.dogaapi.CommandCalledEvent;
import net.diversionmc.discord.dogaapi.DiscordCommand;
import net.diversionmc.discord.managment.music.GuildMusicManager;
import net.diversionmc.discord.managment.music.PlayerControl;
import net.diversionmc.discord.managment.music.TrackScheduler;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.VoiceChannel;
import net.dv8tion.jda.core.exceptions.PermissionException;

public class Live extends DiscordCommand {

    public Live(String name, String... aliases) {
        super(name, aliases);
    }

    @Override
    public void onCommandCalled(CommandCalledEvent e) {
        Guild guild = e.getGuild();

        GuildMusicManager mng = PlayerControl.getMusicManager(guild);

        AudioPlayer player = mng.player;

        TrackScheduler scheduler = mng.scheduler;
        VoiceChannel chan = TheKianBot.getInstance().getVoiceChannelByName("Music", true).get(0);
        if (chan == null)

        {

            e.getChannel().sendMessage("Couldnt connect to channel \"Music\"! Does it exist?").queue();

        } else

        {

            guild.getAudioManager().setSendingHandler(mng.sendHandler);


            try

            {

                guild.getAudioManager().openAudioConnection(chan);

            } catch (PermissionException ex)

            {

                if (ex.getPermission() == Permission.VOICE_CONNECT)

                {

                    e.getChannel().sendMessage("I don't have permission to connect to the Music channel!").queue();

                }

            }

        }

        PlayerControl.loadAndPlay(mng, e.getChannel(), "http://twitch.tv/diversionmc", false);
    }
}
