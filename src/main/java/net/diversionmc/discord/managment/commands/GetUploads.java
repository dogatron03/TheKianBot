package net.diversionmc.discord.managment.commands;

import net.diversionmc.discord.dogaapi.CommandCalledEvent;
import net.diversionmc.discord.dogaapi.DiscordCommand;
import net.diversionmc.discord.utils.Youtube;

public class GetUploads extends DiscordCommand {


    public GetUploads(String name) {
        super(name);
    }


    @Override
    public void onCommandCalled(CommandCalledEvent e) {
        e.getChannel().sendMessage(e.getCommandArgs() + "'s latest upload on youtube is: " + Youtube.getLatestVideo(e.getCommandArgs())).complete();
    }
}
