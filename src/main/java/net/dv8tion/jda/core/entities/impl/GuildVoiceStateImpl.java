/*
 *     Copyright 2015-2017 Austin Keener & Michael Ritter & Florian Spieß
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.dv8tion.jda.core.entities.impl;

import net.dv8tion.jda.core.entities.*;

public class GuildVoiceStateImpl implements GuildVoiceState
{
    private final GuildImpl guild;
    private final Member member;

    private VoiceChannel connectedChannel;
    private String sessionId;
    private boolean selfMuted = false;
    private boolean selfDeafened = false;
    private boolean guildMuted = false;
    private boolean guildDeafened = false;
    private boolean suppressed = false;

    public GuildVoiceStateImpl(GuildImpl guild, Member member)
    {
        this.guild = guild;
        this.member = member;
    }

    @Override
    public boolean isSelfMuted()
    {
        return selfMuted;
    }

    public GuildVoiceStateImpl setSelfMuted(boolean selfMuted)
    {
        this.selfMuted = selfMuted;
        return this;
    }

    @Override
    public boolean isSelfDeafened()
    {
        return selfDeafened;
    }

    public GuildVoiceStateImpl setSelfDeafened(boolean selfDeafened)
    {
        this.selfDeafened = selfDeafened;
        return this;
    }

    @Override
    public JDAImpl getJDA()
    {
        return guild.getJDA();
    }

    @Override
    public AudioChannel getAudioChannel()
    {
        return connectedChannel;
    }

    @Override
    public String getSessionId()
    {
        return sessionId;
    }

    public GuildVoiceStateImpl setSessionId(String sessionId)
    {
        this.sessionId = sessionId;
        return this;
    }

    @Override
    public boolean isMuted()
    {
        return isSelfMuted() || isGuildMuted();
    }

    @Override
    public boolean isDeafened()
    {
        return isSelfDeafened() || isGuildDeafened();
    }

    @Override
    public boolean isGuildMuted()
    {
        return guildMuted;
    }

    public GuildVoiceStateImpl setGuildMuted(boolean guildMuted)
    {
        this.guildMuted = guildMuted;
        return this;
    }

    @Override
    public boolean isGuildDeafened()
    {
        return guildDeafened;
    }

    public GuildVoiceStateImpl setGuildDeafened(boolean guildDeafened)
    {
        this.guildDeafened = guildDeafened;
        return this;
    }

    @Override
    public boolean isSuppressed()
    {
        return suppressed;
    }

    public GuildVoiceStateImpl setSuppressed(boolean suppressed)
    {
        this.suppressed = suppressed;
        return this;
    }

    @Override
    public VoiceChannel getChannel()
    {
        return connectedChannel;
    }

    // -- Setters --

    @Override
    public Guild getGuild()
    {
        return guild;
    }

    @Override
    public Member getMember()
    {
        return member;
    }

    @Override
    public boolean inVoiceChannel()
    {
        return getChannel() != null;
    }

    @Override
    public int hashCode()
    {
        return member.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof GuildVoiceState))
        {
            return false;
        }
        GuildVoiceState oStatus = (GuildVoiceState) obj;
        return this == oStatus || (this.member.equals(oStatus.getMember()) && this.guild.equals(oStatus.getGuild()));
    }

    @Override
    public String toString()
    {
        return "VS:" + guild.getName() + ':' + member.getEffectiveName();
    }

    public GuildVoiceStateImpl setConnectedChannel(VoiceChannel connectedChannel)
    {
        this.connectedChannel = connectedChannel;
        return this;
    }
}
